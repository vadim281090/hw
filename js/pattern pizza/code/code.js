const inpName = document.getElementById('inpName'),
    inpPhone = document.getElementById('inpPhone'),
    inpEmail = document.getElementById('inpEmail'),
    btnReset = document.getElementById('btnReset'),
    btnSubmit = document.getElementById('btnSubmit'),
    banner = document.getElementById('banner');


// Робимо валідацію
function validation(name, phone, email) {

    name.style.border = 'none';
    phone.style.border = 'none';
    email.style.border = 'none';

    if (!/^[А-ЯЁІЇЄ][а-яёіїє]+$/.test(name.value)) { //З великої букви та Кирилиця
        name.style.border = '3px solid red';
    }
    if (!/\+[3]\d{11}/.test(phone.value) || phone.value.length !== 13) { //з "+3" та правильна довжина
        phone.style.border = '3px solid red';
    }
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.value)) { //валідація на правильний емаіл
        email.style.border = '3px solid red';
    }
    if ( // якщо всі поля вірні, то .. 
        name.style.border === 'none' && phone.style.border === 'none' && email.style.border === 'none') {
        location.href = './thank-you.html';
    }
}

//функція очистки полів
function reset() { 
    inpName.value = '';
    inpPhone.value = '';
    inpEmail.value = '';
    inpName.style.border = 'none';
    inpPhone.style.border = 'none';
    inpEmail.style.border = 'none';
}

btnReset.onclick = reset;
btnSubmit.onclick = () => {
    validation(inpName, inpPhone, inpEmail)
};

// втікання кнопки
 banner.onmouseover = function () {
    banner.style.bottom = `${Math.floor(Math.random()*85)}%`;
    banner.style.right = `${Math.floor(Math.random()*85)}%`;
};

//знаходимо куди додавати 
let sauceText = document.getElementById('sauceText'),
    topingsText = document.getElementById('topingsText'),
    totalPrice = document.getElementById('totalPrice'),
    size = 1.5,
    total = 0;

//функція додавання та видалення нових елементів
function nam(x,b,t,img) { 
    let div = document.createElement('div');
    let btn = document.createElement('button');
    btn.style.width = '25px';
    btn.innerText = '  X  ';
    btn.onclick = () => {
        div.remove();
        btn.remove();
        img.remove();
        total -= t;
        totalPrice.textContent = (total*size).toFixed(2) + ' грн';
    }
    div.textContent = x;
    b.append(div);
    div.append(btn);
    total += t;
    totalPrice.textContent = (total*size).toFixed(2) + ' грн';
}

// відстежую клік по інгрідієнтам
const [...ingridients] = document.getElementsByTagName('img');
ingridients.forEach(e => { 
    e.addEventListener('dragstart', function (evt) {
        e.style.border = '3px dotted #000';
        evt.dataTransfer.setData('Text', this.id);
    }, false);
    
    e.addEventListener('dragend', function () {
        this.style.border = '';
    }, false);
})

const target = document.getElementById('target');

target.addEventListener('dragenter', function () {
    this.style.border = '3px solid red';
}, false);

target.addEventListener('dragleave', function () {
  this.style.border = '';
}, true);

target.addEventListener('dragover', function (evt) {
    if (evt.preventDefault) evt.preventDefault();
    return false;
}, false);

//подія дроп
target.addEventListener('drop', function (evt) {

    if (evt.preventDefault) evt.preventDefault();
    if (evt.stopPropagation) evt.stopPropagation();
    this.style.border = '';

    const addd = document.getElementsByClassName('table'),
        img = document.createElement('img'),
        id = evt.dataTransfer.getData('Text');
    
    if (id === 'sauceClassic') {
        img.src = 'Pizza_pictures/sous-klassicheskij_1557758736353.png';
        nam('Кетчуп ', sauceText, 15, img);
    }
    if (id === 'sauceBBQ') {
        img.src = 'Pizza_pictures/sous-bbq_155679418013.png';
        nam('BBQ ', sauceText, 16, img);
    }
    if (id === 'sauceRikotta') {
        img.src = 'Pizza_pictures/sous-rikotta_1556623391103.png';
        nam('Рiкотта ', sauceText, 17, img);
    }
    if (id === 'moc1') {
        img.src = 'Pizza_pictures/mocarela_1556623220308.png';
        nam('Сир звичайний ', topingsText, 23, img);
    }
    if (id === 'moc2') {
        img.src = 'Pizza_pictures/mocarela_1556785182818.png';
        nam('Сир фета ', topingsText, 25,img);
    }
    if (id === 'moc3') {
        img.src = 'Pizza_pictures/mocarela_1556785198489.png';
        nam('Моцарелла ', topingsText, 28, img);
    }
    if (id === 'telya') {
        img.src = 'Pizza_pictures/telyatina_1556624025747.png';
        nam('Телятина ', topingsText, 30, img);
    }
    if (id === 'vetch1') {
        img.src = 'Pizza_pictures/vetchina.png';
        nam('Помідори ', topingsText, 25, img);
    }
    if (id === 'vetch2') {
        img.src = 'Pizza_pictures/vetchina_1556623556129.png';
        nam('Гриби ', topingsText, 22,img);
    }
    
    addd[0].append(img);
    return false
},false)

//радіо перемикач
let [...radio] = document.getElementsByName('size')
radio.forEach(e => { // відстежую клік по радіо
    e.onclick = () => {
        if (e.value === 'big') {
            size = 1.5 //ціна змінюється у % в залежності від розміру
            totalPrice.textContent = (total*size).toFixed(2) + ' грн';
        }
        if (e.value === 'mid') {
            size = 1
            totalPrice.textContent = (total*size).toFixed(2) + ' грн';
        }
        else if (e.value === 'small') {
            size = 0.7
            totalPrice.textContent = (total*size).toFixed(2) + ' грн';
        }
    }
} );
