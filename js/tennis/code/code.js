const ball = document.getElementById('ball'),
    one = document.getElementById('onePlayer'),
    two = document.getElementById('twoPlayer'),
    scoreOne = document.getElementById('scoreOne'),
    scoreTwo = document.getElementById('scoreTwo'),
    btn = document.getElementById('btn'),
    btnEnd = document.getElementById('btnEnd'),
    nameOne = document.getElementById('nameOne'),
    nameTwo = document.getElementById('nameTwo');

let xBall = 500,
    yBall = 350,
    flagX = 'left',
    flagY = 'up',
    flagBtm = 0,
    yOne = 350,
    yTwo = 350,
    scoreOneText = 0,
    scoreTwoText = 0;

//Записуємо Ім'я та виводимо правила
nameOne.textContent = prompt('введіть ім"я першого гравця');
nameTwo.textContent = prompt('введіть ім"я другого гравця');
alert('Перший гравець грає кнопками "W" - вгору та "S" - вниз (не важливо яка мова обрана)');
alert('Другий гравець грає кнопками "стрілка вгору" "стрілка вниз"');
        
//кнопка показати результат та закінчити гру
btnEnd.onclick = () => {
    if (scoreOneText > scoreTwoText) { alert(`Переміг ${nameOne.textContent} з рахунком: ${scoreOneText} - ${scoreTwoText}`) };
    if (scoreOneText < scoreTwoText) { alert(`Переміг ${nameTwo.textContent} з рахунком: ${scoreTwoText} - ${scoreOneText}`) };
    if (scoreOneText === scoreTwoText) { alert(`Нічия`) };
    scoreOneText = 0;
    scoreTwoText = 0;
    scoreOne.textContent = scoreOneText;
    scoreTwo.textContent = scoreTwoText;
};

//Кнопка старт натискається один раз поки не завершиться гра на користь одного з гравя
btn.onclick = () => {
    if (flagBtm === 0) {
        flagBtm = 1;
        btn.style.background = 'gray';
        ball.style.left = 500 + 'px';
        ball.style.left = 350 + 'px';
        xBall = 500;
        yBall = 350;
        yOne = 350;
        yTwo = 350;
        
        let go = setInterval(() => {
        //реалізація відбивання ракетки та зупинка гри якщо промахнувся
            if (flagX == 'right') {
                xBall++;
                ball.style.left = xBall + 'px';
                if (xBall===963 && yTwo<yBall&& yBall<yTwo+100) {
                    flagX = 'left';
                }  

                if (xBall === 983) {
                    scoreOneText++
                    scoreOne.textContent = scoreOneText
                    flagBtm = 0
                    btn.style.background = 'green'
                    clearInterval(go)
                }
        }
        
        if (flagX == 'left') {
            xBall--;
            ball.style.left = xBall + 'px';

            if (xBall === 28 && yOne<yBall&& yBall<yOne+100) {
                console.log(yOne, yBall, yOne + 100);
                flagX = 'right';
            }  
                
            if (xBall === 0) {
                scoreTwoText++;
                scoreTwo.textContent = scoreTwoText;
                flagBtm = 0;
                btn.style.background = 'green';
                clearInterval(go);
            }
        }    
    //реалізація відбивання м'яча вверх та низ
            if (flagY == 'up') {
                yBall--;
                ball.style.top = yBall + 'px';

                if (ball.style.top == '25px') {
                flagY = 'down';
                }  
            }

            if (flagY == 'down') {
                yBall++;
                ball.style.top = yBall + 'px';

                if (ball.style.top == '701px') {
                flagY = 'up';
                }  
            }
            
            one.style.top = yOne + 'px';
            two.style.top = yTwo + 'px';
        }, 5)
    }
}
//управління ракетками
document.addEventListener('keydown', (e) => {
    if (e.code === 'KeyW') {
        yOne -= 55;
        if (yOne <= 50) {
            yOne = 0;
        }
    }
    if (e.code === 'KeyS') {
        yOne += 55;
        if (yOne >= 560) {
            yOne = 600;
        }
    }
    //для другого гравця
    if (e.key == 'ArrowUp') {
        yTwo -= 100;
        if (yTwo <= 90) {
            yTwo = 0;
        }
    }
    if (e.key == 'ArrowDown') {
        yTwo += 100;
        if (yTwo >= 510) {
            yTwo = 600;
        }
    }
})

