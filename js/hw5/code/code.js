//Напишіть функцію isEmpty(obj), яка повертає true, якщо об'єкт не має властивостей, інакше false.

const obj = {
   // num1: 1
}

function isEmpty(x) {
    if (Object.keys(x).length === 0) {
        return true
    }
    else {
        return false
    }
}

console.log(isEmpty(obj))




/*2. разработайте функцию-конструктор, которая будет создавать
объект Human(человек). Создайте массив объектов
и реализуйте функцию, которая будет сортировать элементы массива по 
значению свойства Age по возрастанию или убыванию.*/

function Human (name, age) {
    this.name = name;
    this.age = age;
}

let arr = [];
arr.push(
    new Human('roma1', 24),
    new Human('roma2', 23),
    new Human('roma3', 22),
    new Human('roma4', 21)
)

let sortByAge = (a, b) => a.age > b.age ? 1 : -1;
arr.sort(sortByAge); 
console.log(arr);


/*
3. Разработайте функцию-конструктор, которая будет создавать
объект Human(человек), добавьте на свое усмотрения свойства и методы в этот объект.
Подумайте, какие методы и свойства следует сделать уровня экземпляра. а какие уровня функции-конструктора. */

function Human (name,lastName, age) {
    this.name = name; // властивість екземпляру
    this.lastName = lastName; // властивість екземпляру
    this.age = age; // властивість екземпляру
    this.fullName = function () {  //метод рівня екземпляру, наслідується екземпляром
        return `${this.name} ${this.lastName}`
    }
    
    
    
}

Human.growth = 100; //властивість рівня конструктор, статична властивість

Human.newPerson = function () {  // метод рівня конструктор, статичний метод
    Human.count++;
}

Human.count = 0

let user1 = new Human('petro', 'ivanov', 12);



console.log(user1)



/* Ось такий варіант рахувати кількість створених екземплярів 


function Human (name) {
    populations++;
    this.name = name;
};

let populations = 0;

let n1 = new Human('n1');
let n2 = new Human('n2');
let n3 = new Human('n3');

console.log(populations)// - 3*/

