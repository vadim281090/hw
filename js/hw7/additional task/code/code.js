/*## Задание

Написать реализацию кнопки "Показать пароль".

#### Технические требования:
- В файле `index.html` лежит разметка для двух полей ввода пароля. 
- По нажатию на иконку рядом с конкретным полем - должны отображаться символы, 
    которые ввел пользователь, иконка меняет свой внешний вид.
- Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
- Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)

- По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
- Если значения совпадают - вывести модальное окно (можно alert) с текстом - `You are welcome`;
- Если значение не совпадают - вывести под вторым полем текст красного цвета 
     `Нужно ввести одинаковые значения`
- После нажатия на кнопку страница не должна перезагружаться
- Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.
*/

const inp1 = document.getElementById('inp1');
const inp2 = document.getElementById('inp2');
const btn1 = document.getElementById('btn1');
const textError = document.getElementById('textError');
//функція перевірки
function checkValue () {
    if (inp1.value !== inp2.value) {
        
        textError.textContent = ('Нужно ввести одинаковые значения');
        textError.style.color = 'red';
    }
    else {
        alert('You are welcome')
    }
}

btn1.onclick = checkValue;
//реалізуєму функцію переключення іконки для першого поля
const [...i] = document.getElementsByTagName('i');
    i[0].onclick = function () {
        if (i[0].className !== 'fas fa-eye icon-password') {
            i[0].className = 'fas fa-eye icon-password';
            inp1.type = 'password'   
        }
        else {
            i[0].className = 'fas fa-eye-slash icon-password';
            inp1.type = 'text'
        } 
}
//реалізуєму функцію переключення іконки для другого поля
    i[1].onclick = function () {
        if (i[1].className !== 'fas fa-eye icon-password') {
            i[1].className = 'fas fa-eye icon-password';
            inp2.type = 'password' 
        }
        else {
            i[1].className = 'fas fa-eye-slash icon-password';
            inp2.type = 'text'
        } 
    }





