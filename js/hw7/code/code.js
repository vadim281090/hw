/* При загрузке страницы - показать на ней кнопку с текстом "нарисовать круг".
Данная кнопка должна являться единственным контентом в теле НТМЛ документа, весь остальной 
контент должен быть создан и добавлен на страницу с помощью ЖС.
    При нажатии на кнопку "Нарисовать круг" показать одно поле ввода - диаметр круга.
 при нажатии на кнопку "Нарисовать" создать на странице 100 кругов 10*10 случайного цвета. 
 При клике на конкретный круг - этот круг должен исчезать,
 при этом пустое место заполняться, то все остальные круги сдвигаются влево*/

window.onload = ()=>{
    const inp = document.createElement('input');
    const btn1 = document.getElementById('btn1');
    const btn2 = document.createElement('button');
    const cont = document.getElementById('cont');
    btn2.textContent = 'Нарисовать';
        btn1.onclick = function () {
        inp.placeholder = 'диаметр круга в пикселях';
            document.body.append(inp)
            btn1.remove()
            document.body.append(btn2)
        }

    btn2.onclick = function () {
        for (let i = 0; i < 100; i++) {
            document.body.append(document.createElement('div'))
        }
            let [...div] = document.getElementsByTagName('div');
            div.forEach(element => {
                element.style.background = `hsl( ${Math.floor(Math.random() * 360)}, 100%, 50%)`;
                element.style.width = `${inp.value}px`;
                element.style.height = `${inp.value}px`;
                element.style.borderRadius = '50%';
                element.onclick = function () {
                element.remove()
                }
            });
        cont.style.width = `${(inp.value)*10}px`
        inp.remove()
        btn2.remove()
    }
    
    
}