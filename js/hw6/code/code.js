/*Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. 
Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". 
А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.*/


//ДЗ намагався зробити згідно малюнку. Там були трохи інші поля


class Person {
    constructor(param) {
        this.age = param.age;
        this.fullName = param.fullName;
    }
    toString() {
        console.log(this);
    }
}


class Driver extends Person {
    constructor(param) {
        super (param)
        this.experiens = param.experiens;
    }
    toString() {
        console.log(this);
    }
}


class Engine {
    constructor(param) {
        this.power = param.power;
        this.company = param.company;
    }
    
    toString() {
        console.log(this);
    }
}

class Car {
    constructor(param, Driver, Engine,) {
        this.marka = param.marka;
        this.carClass = param.carClass;
        this.driver = Driver;
        this.engine = Engine;
  }

  start () {
    console.log(`Поїхали`);
    }
    stop() {
        console.log(`Зупиняємося`);
    }
    turnRight() { 
        console.log(`Поворот праворуч`);
    }
    turnLeft() {
        console.log(`Поворот ліворуч`);
    }
    toString() {
        console.log(this);
    }
}



class Lorry extends Car {
    constructor(x) {
        super(x)
        this.carrying = x.carrying;
    }
    toString() {
        console.log(this);
    }
}

class SportCar extends Car {
    constructor(x) {
        super(x)
        this.speed = x.speed;
    }
    toString() {
        console.log(this);
    }
}


// тут я так і не зрозумів як записати данні в властивість driver через конструктор, тому трохи змахлював. 

const car1 = new SportCar ({
 
    carClass: 'sport Car',
    company: 'bmw',
    marka: 'x5',
    speed: '200km',
})

car1.driver = {
    age: 18,
  fullName: 'ivanov vladimir',
    experiens: 2,
}

car1.engine = {
    company: 'bmw',
    power: 300,  
}

car1.toString()