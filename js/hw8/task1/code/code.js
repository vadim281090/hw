//таймер

const btnStart = document.getElementById("start"), 
btnStop = document.getElementById("stop"), 
    sec = document.getElementById("sec"),
    min = document.getElementById("min"),
    hour = document.getElementById("hour"),
btnReset = document.getElementById("reset");
const stopwatch = document.querySelector(".container-stopwatch");

let startTime = undefined;
let second = '00';
let minut = '00';
let hours = '00';

let flag = 0; // реалізація заборони подвійного натиску кнопки старт

btnStop.onclick = () =>{
    clearClass();
    stopwatch.classList.add("red");
    clearInterval(startTime); //стоп відліку
    flag = 0;
    
};
btnStart.onclick = () => {

if (flag === 0) {
    clearClass();
    stopwatch.classList.add("green"); 
    startTime = setInterval (() => {
        second++;
     if (second < 10) {
        second = `0${second}`
    };
        
    if (second > 59) {
        second = '00'
        minut++
        if (minut <10) {
            minut = `0${minut}`
        };
       
        if (minut > 59) {
            minut = '00'
            hours++
            if (hours <10) {
            hours = `0${hours}`
            }; 
        }
    }
        showInterval();
    }, 1000);
        flag = 1;
    }
};

btnReset.onclick = () =>{
    clearClass();
    stopwatch.classList.add("silver");
    second = '00';
    minut = '00';
    hours = '00';
    showInterval()
    flag = 0;
}
function clearClass () {
    stopwatch.classList.remove("red");
    stopwatch.classList.remove("black");
    stopwatch.classList.remove("green");
    stopwatch.classList.remove("silver");
}
function showInterval() {
    sec.innerText = second;
    min.innerText = minut;
    hour.innerText = hours;
}
