/*Напиши функцию map(fn, array), которая принимает на вход функцию и массив,
 и обрабатывает каждый элемент массива этой функцией, возвращая новый массив.
*/

function map(f, a) {
    
    for (let i = 0; i < a.length; i++) {
        a[i] = f(a[i]);
    }
    return a;
}

function fn(x) {
    return x * 3;
}

const array = [1, 2, 3, 4, 5];

console.log(map(fn, array));


/*
Задача 2
Перепишите функцию, используя оператор '?' или '||'
Следующая функция возвращает true, если параметр age больше 18. 
В ином случае она задаёт вопрос confirm и возвращает его результат.
1 function checkAge(age) {
2 if (age > 18) {
3 return true;
4 } else {
5 return confirm('Родители разрешили?');
6 } }*/


let age = parseFloat(prompt('age'));
function checkAge() { 
   return result = age >= 18 ? true : confirm('Родители разрешили?');
}
console.log(checkAge(age))
