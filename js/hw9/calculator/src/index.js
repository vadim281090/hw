const [...btnRed] = document.getElementsByClassName('pink'),
    res = document.getElementById('res'),
    reset = document.getElementById('reset'),
    [...btn] = document.getElementsByClassName('black'),
    [...mem] = document.getElementsByClassName('gray'),
    mrc = document.getElementById('mrc'),
    display = document.querySelector(".display > input");
let flag = 0,
    flagMrc = 0;


//кнопки пам'яті
mem.forEach(el => {
    el.onclick = () => {

        switch (el.value) { 
            case 'm+': if (el.value === 'm+') // додає до записаного в пам'яті числа
                
                calc.mr = Number(display.value) + Number(calc.mr)
                calc.value1 = calc.mr
                flag = 1 
                show(calc.mr, display)
                break

            case 'm-': if (el.value === 'm-')//віднімає від записаного в пам'яті числа
                calc.mr = Number(calc.mr) - Number(display.value)
                calc.value1 = calc.mr
                flag = 1 
                show(calc.mr, display)
                break
                
            
            case 'mrc': if (el.value === 'mrc') 
                if (flagMrc === 0) { // записує число в пам'ять. Виводить на єкран, якщо натиснута була кнопка "С"
                    calc.mr = display.value;
                    //calc.value1 = '';
                    flagMrc = 1;
                    mrc.textContent = 'mrc';
                }
                else { // другий натиск, якщо в пам'яті вже було записано число обнуляє
                    show(calc.mr,display)
                    calc.mr = 0
                    flagMrc = 0
                    mrc.textContent = '';
                }
    }
}
})

btn.forEach(el => { //натиск клавіш з цифрами та вивід на екран
    el.onclick = () => {
        if (flag === 0) {
            calc.value1 += el.value
            show(calc.value1, display)
           return calc.value1
        }
        else {
            calc.value2 += el.value
            show(calc.value2, display) 
            res.disabled = false
            return calc.value2
        }
    }
})
                    
reset.onclick = () => { //кнопка ресет, обнуляє все окрім записаного в пам'ять
    flag = 0;
    addRes()
    calc.value1 = '';
    show(0, display)
}

btnRed.forEach(el => {  //кнопки дії перемикають запис між value1 i value2
    el.onclick = () => {
        flag = 1;
        calc.action = el.value
        show(0, display)
    }
})

const calc = {
    value1 : "",
    value2: "",
    action: '',
    mr: '',
}

res.onclick = () => { //не працює повтор операції при повторному натиску = для цього потрібно переписати кнопки дії

    show(calcRes(calc.value1, calc.value2, calc.action), display)
    calc.value1 = calcRes(calc.value1, calc.value2, calc.action)
    addRes()
}

function addRes() { //для зберігання результату і можливості продовження дій після нажатого "="
    calc.value2 = "",
    calc.action = '';   
}

function calcRes(x, y, z) { //арифметичні дії
    switch (z) {
        case '+': if (z === '+')
            return Number(x) + Number(y)

        case '-': if (z === '-')
            return Number(x) - Number(y)
            
        case '*': if (z === '*')
            return Number(x) * Number(y)
            
        case '/': if (z === '/')
            return Number(x) / Number(y)
    }
}


function show (value, el) {
 el.value = value
}